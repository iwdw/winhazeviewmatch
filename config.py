import os
import json


class Struct:
	def __init__(self, **kwargs):
		self.__dict__.update(kwargs)

def loadParams(f):
	cf = f + "\config.json"
	if os.path.exists(cf):
		jsonFile = open(cf)
		d = json.load(jsonFile)
		s = Struct(**d)
		return s
	else:
	    print "No Config File Found %s"
            return None

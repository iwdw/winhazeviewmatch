import numpy as np
from osgeo import gdal
from osgeo import osr, ogr
from RayCastDEMQuadTree import getRotationMatrix
from DEMOctTree import ray 
import urllib2
import json
import os
from zipfile import ZipFile

############################################################
# Input Parameters 
############################################################



def getDEMFiles(obsLat, obsLon, viewRange, heading, zipDir, elevDir):
	"""
		Retreives a set of DEM files from the national map
		A Bounding box is computed using obsLat, obsLon and the point which is at viewRange meters from the observation point along the given heading
		DEM zip files are downloaded to zipDir
		DEM zip files are extracted into elevDir
		The return value is a list of .flt files begining with the file containing the observation point
	"""
	############################################################
	# Compute Bounding Box
	############################################################
	print "Computing Area of Interest"
	geoSr = osr.SpatialReference()
	geoSr.ImportFromEPSG(4326)
	lccSR = osr.SpatialReference()
	lccSR.ImportFromProj4('+proj=aeqd  +lat_0=%0.12f +lon_0=%0.12f +units=m'%(obsLat, obsLon))
	transform = osr.CoordinateTransformation(geoSr, lccSR)
	transformBack = osr.CoordinateTransformation(lccSR, geoSr)

	obsPoint = ogr.Geometry(ogr.wkbPoint)
	obsPoint.AddPoint( obsLon, obsLat)
	obsPoint.Transform(transform)

	viewVector = np.matrix([[0],[1],[0]])
	viewVector = getRotationMatrix(-1.0*heading, 0.0, 0.0)*viewVector

	viewRay = ray([obsPoint.GetX(), obsPoint.GetY(), 0.0], viewVector)

	p2 = viewRay.getPoint(viewRange)
	pt2 = ogr.Geometry(ogr.wkbPoint)
	pt2.AddPoint(p2[0].item(), p2[1].item())
	pt2.Transform(transformBack)
	obsPoint.Transform(transformBack)

	lon1 = min(pt2.GetX(), obsPoint.GetX())
	lon2 = max(pt2.GetX(), obsPoint.GetX())
	lat1 = max(pt2.GetY(), obsPoint.GetY())
	lat2 = min(pt2.GetY(), obsPoint.GetY())



	############################################################
	# Retrieve DEM file listing for bounding box
	############################################################
	print "Retrieving DEM File listing from USGS"
	url = "https://viewer.nationalmap.gov/tnmaccess/api/searchProducts?bbox=%0.6f,%0.6f,%0.6f,%0.6f&datasets=National+Elevation+Dataset+(NED)+1/3+arc-second&prodFormats=GridFloat"

	# get obs point file first
	f = urllib2.urlopen(url%(obsPoint.GetX(), obsPoint.GetY(), obsPoint.GetX(), obsPoint.GetY()))
	d = json.loads(f.read())
	f.close()
	assert (len(d['items']) == 1)
	firstDEMZip = os.path.basename(d['items'][0]['downloadURL'])

	# get all files in bbox
	f = urllib2.urlopen(url%(lon1, lat1, lon2, lat2))
	d = json.loads(f.read())
	f.close()

	############################################################
	# Download DEM files
	############################################################
	print "Downloading DEM Files"
	dlzips = []
	for i in d['items']:
		fn = os.path.basename(i['downloadURL'])
		outFile = zipDir+fn
		dlzips.append(outFile)
		if os.path.exists(outFile):
			print "Already Downloaded %s"%outFile
		else:
			print "Downloading %s"%fn
			f = urllib2.urlopen(i['downloadURL'])
			with open(outFile, "wb") as local_file:
					local_file.write(f.read())
			f.close()

	############################################################
	# Extract DEM archives
	############################################################
	print "Extracting DEM Files"
	firstDEMFLT = ''
	elevFiles = []
	for f in dlzips:
		z1 = ZipFile(f)
		l = z1.namelist()
		for i in l:
			if i[-4:] == '.flt':
				if os.path.basename(f) == firstDEMZip:
					elevFiles.insert(0, elevDir+i)
				else:
					elevFiles.append(elevDir+i)
				break
		if not os.path.exists(elevDir+i):
			print "Extracting %s"%f
			z1.extractall(elevDir)
		else:
			print "Already Extracted %s"%f
		z1.close()
	return elevFiles

if __name__ == "__main__":
	obsLat = 46.785246
	obsLon = -121.742252
	obsElev = 1750
	viewRange = 100000
	heading = 315
	zipDir = "./ElevZips/"
	elevDir = "./ElevExtract/"	
	elevFiles = getDEMFiles(obsLat, obsLon, obsElev, viewRange, heading, zipDir, elevDir)





import sys
import os, inspect
workingdir = '\\'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('\\')[0:-2]) + "\\"
os.environ["GDAL_DATA"] = "C:\\Enthought\\User\\share\\gdal\\"
sys.path.append('../')
sys.path.append(workingdir)
print workingdir
import config
#import DEMToPly as d
#import DEMToSTL as dstl
import RayCastDEMQuadTree as r
import numpy as np
import rectify as rct
import showranges
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
#import DEMOctTree as tree
import time
import gdal
import math
import json
import inspect
from gui import RangeFixer
from PIL import Image
import DEMRetrieval as demGet

params = config.loadParams(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

def getFileName(baseStr, extension, param=None):
	if param is None:
		param = params
	baseStr += ".obsLat_%0.8f.obsLon_%0.8f.obsElev_%0.3f.horizAngle_%0.3f.elevAngle_%0.3f.heading_%0.3f"%(param.observationPoint[1], param.observationPoint[0], param.observationPoint[2], param.horizAngleEstimate, param.elevationAngleEstimate, param.headingEstimate)
	baseStr += "."+ extension
	return baseStr

def loadView(elevFile):
	doProjection = True
	if os.path.exists(elevFile.replace('.flt','_reproj.obsLat_%0.8f.obsLon_%0.8f.flt'%(params.observationPoint[1], params.observationPoint[0]))):
		elevFile = elevFile.replace('.flt','_reproj.obsLat_%0.8f.obsLon_%0.8f.flt'%(params.observationPoint[1], params.observationPoint[0]))
		doProjection = False
	v = r.View(obsLat=params.observationPoint[1], 
				 obsLon=params.observationPoint[0], 
				 obsElev=params.observationPoint[2], 
				 horizAngle=params.horizAngleEstimate,
				 heading=params.headingEstimate,
				 elevationAngle=params.elevationAngleEstimate,
				 elevFile = elevFile, 
				 maxRange=params.maxRange,
				 projectDEM=doProjection)

	if doProjection:
		driver = gdal.GetDriverByName("GTiff")
		dst_ds = driver.CreateCopy( elevFile.replace('.flt','_reproj.obsLat_%0.8f.obsLon_%0.8f.flt'%(params.observationPoint[1], params.observationPoint[0])), v.geo, 0 )
	return v

def doRays(viewObj=None, initRangeFile = None, initElevFile = None, outRangeFile=None, outElevFile=None, outRangeImage=None, outElevImage=None):
	t0 = time.time()
	

	v = None
	if viewObj is not None:
		v = viewObj
	else:
		v = loadView()				


	initRange = None
	initElev = None
	if initRangeFile is not None:
		initRange = np.load(initRangeFile)
	else:
		initRange = np.zeros((params.matrixHeight, params.matrixWidth))
	if initElevFile is not None:
		initElev = np.load(initElevFile)
	else:
		initElev = np.zeros((params.matrixHeight, params.matrixWidth))

	initRange[np.isnan(initRange)] = 0.0
	initElev[np.isnan(initElev)] = 0.0
	ranges, elevs = v.computeRanges(prevGrid=initRange, prevElevs = initElev, arrayDumpName = outRangeFile, elevDumpName = outElevFile)

	
	np.save(rangeName, ranges)
	np.save(elevName, elevs)
	mpimg.imsave(outRangeImage, np.flipud(ranges))
	mpimg.imsave(outElevImage, np.flipud(elevs))
	i, p = v.bbtree.getIters()
	t1 = time.time()
	print "\n\nRange Computation Time Elapsed %0.3f"%(t1-t0)

	print "Total iterations %d for %d points"%(i,p)
	print "Average iterations %0.2f " % (i / float(p))

def doCurveCorrection(viewObj, initRangeFile, initElevFile, outRangeFile, outElevFile, outRangeImage, outElevImage):
	t0 = time.time()
	v = None
	if viewObj is not None:
		v = viewObj
	else:
		v = loadView()	

	prev = np.load(initRangeFile)
	prevElev = np.load(initElevFile)
	prev, prevElev = v.curvatureCorrection(prev, prevElev)

	np.save(outRangeFile, prev)
	np.save(outElevFile, prevElev)
	mpimg.imsave(outRangeImage, np.flipud(prev))
	mpimg.imsave(outElevImage, np.flipud(prevElev))
	t1 = time.time()
	print "\n\nCurve Correction Time Elapsed %0.3f"%(t1-t0)



def doSkyline(viewObj = None, initRangeFile=None, initElevFile=None):
	t0 = time.time()
	v = None
	if viewObj is not None:
		v = viewObj
	else:
		v = loadView()


	initRange = initElev = None


	if initRangeFile is not None:
		initRange = np.load(initRangeFile)
	else:
		initRange = np.zeros((params.matrixHeight, params.matrixWidth))
	if initElevFile is not None:
		initElev = np.load(initElevFile)
	else:
		initElev = np.zeros((params.matrixHeight, params.matrixWidth))

	ranges, elevs = v.doSkyline(initRange, initElev)
	skynameImg = getFileName("images/skyline", "png")
	skyname = getFileName("arrays/skyline", "npy")
	elevName = getFileName("arrays/elevation", "npy")

	mpimg.imsave(skynameImg, np.flipud(ranges))
	np.save(skyname, ranges)
	np.save(elevName, elevs)
	t1 = time.time()
	print "\n\nSkyline Time Elapsed %0.3f"%(t1-t0)
	return v


def showSkyline():
	skyname = getFileName("arrays/skyline", "npy")
	showranges.showRanges(skyname)

def showRanges():
	rangeName = getFileName("arrays/range", "3_rectified.npy")
	showranges.showRanges(rangeName, True)

def showElev():
	elevName = getFileName("arrays/elevation", "npy")
	showranges.showRanges(elevName)

def showTerrain():
	d.renderPolyData(params.terrainFile, params.obsPoint[2], params.heading, params.elevationAngle, params.vertAngle)
	#dstl.renderPolyData(params.terrainFile.replace('.ply','.stl'), params.obsPoint[2], params.heading, params.elevationAngle, params.vertAngle)

def launchRectificationUI():
	im = mpimg.imread(params.imageFile)
	rangeName = getFileName('arrays/range','2_curved.npy')
	elevName = getFileName('arrays/elevation','2_curved.npy')
	ranges = np.load(rangeName)
	ranges[np.isnan(ranges)] = 0
	ranges = np.flipud(ranges)
	elev = np.load(elevName)
	elev = np.flipud(elev)
	correspondenceFile = getFileName('correspondence', 'json')
	rct.showClicker(im, ranges,elev, initialPointsFile = correspondenceFile, outputPointsFile=correspondenceFile)

def doRectification():
	rangeName = getFileName('arrays/range','2_curved.npy')
	elevName = getFileName('arrays/elevation','2_curved.npy')
	elevNameOut = getFileName("arrays/elevation", "3_rectified.npy")
	rangeNameOut = getFileName("arrays/range", "3_rectified.npy")
	elevNameOutImg = getFileName("images/elevation", "3_rectified.png")
	rangeNameOutImg = getFileName("images/range", "3_rectified.png")
	correspondenceFile = getFileName('correspondence', 'json')
	rct.rectify2(rangeName, params.imageFile, correspondenceFile, outGridName=rangeNameOut, outImageName=rangeNameOutImg)
	rct.rectify2(elevName, params.imageFile, correspondenceFile, outGridName=elevNameOut, outImageName=elevNameOutImg)

def getDEMFiles():
	# obsLat=params.obsPoint[1], 
	# 			 obsLon=params.obsPoint[0], 
	# 			 obsElev=params.obsPoint[2], 
	# 			 horizAngle=params.horizAngle,
	# 			 vertAngle=params.vertAngle,
	# 			 heading=params.heading,
	# 			 elevationAngle=params.elevationAngle,
	# 			 elevFile = elevFile, 
	# 			 terrainFile=params.terrainFile, 
	# 			 maxRange=params.maxRange,
	# 			 projectDEM=doProjection
	return demGet.getDEMFiles(params.observationPoint[1], params.observationPoint[0], params.maxRange, params.headingEstimate, './elev/zips/', './elev/')

def doLowResRayTrace(viewObj=None):
	t0 = time.time()
	param = params
	elevName = getFileName("arrays/elevation", "0_estimate.npy", param=param)
	rangeName = getFileName("arrays/range", "0_estimate.npy", param=param)
	elevNameImg = getFileName("images/elevation", "0_estimate.png", param=param)
	rangeNameImg = getFileName("images/range", "0_estimate.png", param=param)
	elevFiles = getDEMFiles()
	v = None
	if viewObj is not None:
		v = viewObj
	else:
		v = loadView(elevFiles[0])
	
	im = mpimg.imread(params.imageFile)


	upVector = None
	viewVector = None
	imageVertAngle = None
	poseFile = getFileName("cameraPose", "json")
	elevs = None
	ranges = None
	elevs = np.zeros((params.initMatrixHeight,params.initMatrixWidth))
	ranges = np.zeros((params.initMatrixHeight,params.initMatrixWidth))

	print "Computing low res ranges"


	v = None
	for elevFile in elevFiles:
		print "\n\nProcessing Elevation file %s"%elevFile
		reprojFile = elevFile.replace('.flt','_reproj.obsLat_%0.8f.obsLon_%0.8f.flt'%(params.observationPoint[1], params.observationPoint[0]))
		projectDEM = True
		if os.path.exists(reprojFile):
			print "\n\nProjected DEM exists"
			elevFile = reprojFile
			projectDEM = False
		else:
			print "\n\nWill project DEM"

		#Create a new view using the new ViewVector, upVector and vertical angle (with some padding)
		v = r.View(obsLat=params.observationPoint[1], 
			 obsLon=params.observationPoint[0], 
			 obsElev=params.observationPoint[2], 
			 horizAngle=params.horizAngleEstimate,
			 aspectRatio=params.aspectRatio,
			 heading=params.headingEstimate,
			 elevationAngle=params.elevationAngleEstimate,
			 elevFile = elevFile, 
			 projectDEM=projectDEM)
		
		if projectDEM:
			driver = gdal.GetDriverByName("GTiff")
			dst_ds = driver.CreateCopy( reprojFile, v.geo, 0 )

		ranges[np.isnan(ranges)] = 0.0
		elevs[np.isnan(elevs)] = 0.0

		ranges, elevs = v.computeRanges(prevGrid=ranges, prevElevs = elevs, arrayDumpName = rangeName, elevDumpName = elevName)

	np.save(rangeName, ranges)
	np.save(elevName, elevs)
	mpimg.imsave(rangeNameImg, np.flipud(ranges))
	mpimg.imsave(elevNameImg, np.flipud(elevs))

	elevName = getFileName("arrays/elevation", "0_estimate_curved.npy", param=param)
	rangeName = getFileName("arrays/range", "0_estimate_curved.npy", param=param)
	elevNameImg = getFileName("images/elevation", "0_estimate_curved.png", param=param)
	rangeNameImg = getFileName("images/range", "0_estimate_curved.png", param=param)
	
	
	ranges, elevs = v.curvatureCorrection(ranges, elevs)
	np.save(rangeName, ranges)
	np.save(elevName, elevs)
	mpimg.imsave(rangeNameImg, np.flipud(ranges))
	mpimg.imsave(elevNameImg, np.flipud(elevs))

def estimatePose():

	print "Loading Correspondence UI"

	elevName = getFileName("arrays/elevation", "0_estimate_curved.npy", param=params)
	rangeName = getFileName("arrays/range", "0_estimate_curved.npy", param=params)
	ranges = np.load(rangeName)
	elevs = np.load(elevName)
	ranges = np.flipud(ranges)
	elevs = np.flipud(elevs)
	im = mpimg.imread(params.imageFile)
	elevFiles = getDEMFiles()
	v = loadView(elevFiles[0])
	poseFile = getFileName("cameraPose", "json")

	initCorrespondenceFile = getFileName('initCorrespondence', 'json')
	if not os.path.exists(initCorrespondenceFile):
		f = open(initCorrespondenceFile, "w")
		p = {"RangePoints":[], "ImagePoints":[[int(math.floor(im.shape[1] / 2.0)), int(math.floor(im.shape[0] / 2.0))]]}
		json.dump(p,f)
		f.close()
	rct.showClicker(im,ranges,elevs, initialPointsFile = initCorrespondenceFile, outputPointsFile=initCorrespondenceFile)
	
	print "Processing Correspondence"
	f = open(initCorrespondenceFile)
	p = json.load(f)

	#make sure that a range point exists
	assert (len(p['RangePoints']) >=2 and len(p['ImagePoints']) >= 2 and len(p['RangePoints']) ==  len(p['ImagePoints']))
	cp = p['RangePoints'][0] #x,y  of first range point is the new center of the viewing plane
	viewVector = v.computeRay(cp[1], cp[0], (params.initMatrixHeight,params.initMatrixWidth))#get the ray for new center, this ray's view vector is the new viewVector
	viewVector = viewVector.npView

	#the new up vector will be orthogonal to the center ray and some other ray from the same row
	otherVector = v.computeRay(cp[1], 0, (params.initMatrixHeight,params.initMatrixWidth))
	upVector = np.cross(viewVector, otherVector.npView)
	upVector = upVector / np.linalg.norm(upVector)

	#compute the vertical viewing angle:
	#	get vertical angle between range points 0 and 1
	#		rangeDegPerPixel = (vertAngle / HeightInPixels)
	rangeDegPerPixel = params.horizAngleEstimate / float(params.initMatrixWidth)
	#		vertAngleBetweenPoints = rangeDegPerPixel * abs(pt[0].y-pt[1].y)
	horizAngleBetweenPoints = rangeDegPerPixel * math.fabs(p['RangePoints'][0][0] - p['RangePoints'][1][0])
	#		imageDegPerPixel = vertAngleBetweenPoints / abs(imgPt[0].y - imgPt[1].y)
	imageDegPerPixel = horizAngleBetweenPoints / math.fabs(p['ImagePoints'][0][0] - p['ImagePoints'][1][0])
	#		imageVertAngle = imageDegPerPixel * imgHeightInPixels
	imageHorizAngle = imageDegPerPixel * im.shape[1] + 4

	#dump the details to a file:
	pose = {"viewVector": viewVector.tolist(), "upVector":upVector.tolist(), "horizAngle":imageHorizAngle}
	f = open(poseFile, "w")
	json.dump(pose, f)
	f.close()


def doDetailedRayTrace():
	im = mpimg.imread(params.imageFile)
	ranges = elevs = None
	rName = getFileName('arrays/range','1_detailed.npy')
	eName = getFileName('arrays/elevation','1_detailed.npy')
	rNameI = getFileName('images/range','1_detailed.png')
	eNameI = getFileName('images/elevation','1_detailed.png')
	if ranges is None:
		if os.path.exists(rName):
			ranges = np.load(rName)#np.zeros((im.shape[0], im.shape[1]))
		else:
			ranges = np.zeros((im.shape[0], im.shape[1]))
	if elevs is None: 
		if os.path.exists(eName):
			elevs = np.load(eName)
		else:
			elevs = np.zeros((im.shape[0], im.shape[1]))

	poseFile = getFileName("cameraPose", "json")
	if not os.path.exists(poseFile):
		esitmatePose()

	f = open(poseFile, "r")
	pose = json.load(f)
	upVector = np.array(pose["upVector"])
	viewVector = np.array(pose["viewVector"])
	imageHorizAngle = np.array(pose["horizAngle"])

	v = None
	elevFiles = getDEMFiles()
	for elevFile in elevFiles:
		print "\n\nProcessing Elevation file %s"%elevFile
		reprojFile = elevFile.replace('.flt','_reproj.obsLat_%0.8f.obsLon_%0.8f.flt'%(params.observationPoint[1], params.observationPoint[0]))
		projectDEM = True
		if os.path.exists(reprojFile):
			print "\n\nProjected DEM exists"
			elevFile = reprojFile
			projectDEM = False
		else:
			print "\n\nWill project DEM"

		#Create a new view using the new ViewVector, upVector and vertical angle (with some padding)
		v = r.View(obsLat=params.observationPoint[1], 
					 obsLon=params.observationPoint[0], 
					 obsElev=params.observationPoint[2], 
					 horizAngle=imageHorizAngle,
					 viewVector=viewVector,
					 upVector=upVector,
					 elevFile = elevFile, 
					 projectDEM=projectDEM)
		if projectDEM:
			driver = gdal.GetDriverByName("GTiff")
			dst_ds = driver.CreateCopy( reprojFile, v.geo, 0 )



		ranges, elevs = v.computeRanges(prevGrid=ranges, prevElevs = elevs, divideGrid=False, arrayDumpName=rName, elevDumpName=eName)
		mpimg.imsave(rNameI, np.flipud(ranges))
		mpimg.imsave(eNameI, np.flipud(elevs))
		ranges[np.isnan(ranges)] = 0.0
		elevs[np.isnan(elevs)] = 0.0


	print "\n\nPerforming Curvature Correction"
	ranges, elevs = v.curvatureCorrection(ranges, elevs)
	rName = getFileName('arrays/range','2_curved.npy')
	eName = getFileName('arrays/elevation','2_curved.npy')
	rNameI = getFileName('images/range','2_curved.png')
	eNameI = getFileName('images/elevation','2_curved.png')
	mpimg.imsave(rNameI, ranges)
	mpimg.imsave(eNameI, elevs)
	np.save(rName, ranges)
	np.save(eName, elevs)

	# print "\n\nCollection Rectification Reference Points"
	# rectify()

	# print "\n\nPerforming Rectification"
	# rectify2()

	t1 = time.time()
	print "\n\n\Complete %0.3f"%(t1-t0)


def fixRanges():
	outRangeFile = getFileName('arrays/range','4_final.bmp')
	inRangeFile = getFileName('arrays/range','4_final.npy')
	outRangeImage = getFileName('images/range', '4_final.png')
	outHazeImage = getFileName('images/haze', '5_haze_%sm.png')

	r = RangeFixer(imageFile=params.imageFile, rangeFile=inRangeFile, outRangeFile=outRangeFile, outRangeImage=outRangeImage, outHazeImage=outHazeImage)

def convertMask():
	#rangeFile='./GrandCanyon1/arrays/reduced.npy'
	rangeFile = getFileName('arrays/range','4_final.npy')
	rangeFileOut = getFileName('arrays/range','5_converted.txt')

	r = np.load(rangeFile)
	f = open(rangeFileOut,'w')

	for row in range(r.shape[0]):
		for col in range(r.shape[1]):
			f.write("%d,"%r[row,col])
		f.seek(-1,1)
		f.write("\n")

	f.close()
	imageOut = getFileName('images/range','5_converted.bmp')

	maxRange = r.max()
	r[r==0] = np.nan
	r *= 254.0/maxRange #scale ranges to 0-254
	cleanMask(r)
	r[np.isnan(r)] = 255.0
	red = np.zeros(r.shape)
	rgbArray = np.zeros((r.shape[0],r.shape[1],3), 'uint8')
	rgbArray[..., 0] = red*256 #red
	rgbArray[..., 1] = r #green
	rgbArray[..., 2] = red*256 #blue
	img = Image.fromarray(rgbArray)
	img.save(imageOut)
	#mpimg.imsave(imageOut, r)

def cleanMask(mask):
	oSsky = True
	for col in range(mask.shape[1]):
		onSky = True
		for row in range(mask.shape[0]):
			if onSky and np.isnan(mask[row][col]): #found more sky
				continue
			elif onSky and not np.isnan(mask[row, col]):
				onSky = False
			elif not onSky and np.isnan(mask[row,col]): #bad value, make it the average of above/below neighbors
				mask[row,col] = 254



if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("--dem", help="Retrieve digital elevation models",action="store_true")
	parser.add_argument("--lowresraytrace", help="Execute low resolution ray trace for camera pose refinement",action="store_true")
	parser.add_argument("--poseestimateui", help="Display the camera pose refinement user interface",action="store_true")
	parser.add_argument("--detailedraytrace", help="Execute detailed ray trace",action="store_true")
	parser.add_argument("--rectifyui", help="Launch perspective rectification tool",action="store_true")
	parser.add_argument("--rectify", help="Compute homography and perform rectifcation",action="store_true")
	parser.add_argument("--artifactui", help="Launch artifact correction tool",action="store_true")
	parser.add_argument("--convertmask", help="Convert mask to WinHaze ready format",action="store_true")

	args = parser.parse_args()

	t0 = time.time()
	if args.dem:
		getDEMFiles()
	if args.lowresraytrace:
		doLowResRayTrace()
	if args.poseestimateui:
		estimatePose()
	if args.detailedraytrace:
		doDetailedRayTrace()
	if args.rectifyui:
		launchRectificationUI()
	if args.rectify:
		doRectification()
	if args.artifactui:
		fixRanges()
	if args.convertmask:
		convertMask()



	#doTerrain()
	#showTerrain()
	#v = doSkyline()
	#showSkyline()
	


	#doCurveCorrection()
	#testMask()
	#estimatePose()
	#showElev()
	#rectify()
	#rectify2()
	#showRanges()
	#showElev()
	#fixRanges()
	t1 = time.time()
	print "\n\nTotal Time elapsed %0.4f"%(t1-t0)
	# v = r.View(obsLat=params.obsPoint[1], 
	# 		 obsLon=params.obsPoint[0], 
	# 		 obsElev=params.obsPoint[2], 
	# 		 horizAngle=params.horizAngle,
	# 		 vertAngle=params.vertAngle,
	# 		 heading=params.heading,
	# 		 elevationAngle=params.elevationAngle,
	# 		 elevFile = params.elevFile, 
	# 		 terrainFile=params.terrainFile, 
	# 		 maxRange=params.maxRange)

	# b = v.bbtree 
	# r = tree.ray([0.0,0.0,2155.0], [-0.96771441,  0.15115911, -0.20169219])

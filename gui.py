import Tkinter as tkinter
import tkMessageBox
from PIL import Image, ImageTk
import os
import numpy as np
import scipy.misc as sm
import matplotlib.image as mpimg


class RangeFixer():
	def __init__(self, **kwargs):
		self.imageFile = kwargs.get('imageFile')
		self.rangeFile = kwargs.get('rangeFile')
		self.outRangeImage = kwargs.get('outRangeImage')
		self.root = tkinter.Tk()
		self.root.resizable(0,0)
		self.unsavedChanges = False
		self.root.protocol("WM_DELETE_WINDOW", self._deleteWindow)
		
		if self.rangeFile[-4:] == '.npy':
			self.rangeMatrix = np.load(self.rangeFile)
			self.rangeMatrix[np.isnan(self.rangeMatrix)] = 0.0
		elif self.rangeFile[-4:] =='.bmp':
			im = Image.open(self.rangeFile)
			p = np.array(im)
			self.rangeMatrix = p[...,1].astype(float)

		self.alphaMatrix = np.array(self.rangeMatrix)
		self.outRangeFile = kwargs.get('outRangeFile')
		self.outHazeImage = kwargs.get('outHazeImage')

		self.masterImg = Image.open(self.imageFile)
		self.viewLeft = 0
		self.viewRight = self.masterImg.size[0]
		self.viewTop = 0
		self.viewBottom = self.masterImg.size[1]
		self.viewWidth = self.viewRight - self.viewLeft
		self.viewHeight = self.viewBottom - self.viewTop

		self.frame = tkinter.Frame(self.root, height=self.masterImg.size[1],width=self.masterImg.size[0])
		self.canvas = tkinter.Canvas(self.frame, height=self.masterImg.size[1],width=self.masterImg.size[0], cursor="tcross")
		self.maxVal = np.max(self.rangeMatrix)
		self.minVal = np.min(self.rangeMatrix[self.rangeMatrix != 0])
		self.span = self.maxVal-self.minVal
		#print self.maxVal
		#print self.minVal

		self.alphaImg = Image.new('RGBA', (self.rangeMatrix.shape[1], self.rangeMatrix.shape[0]), "green")
		self.alphaPix = self.alphaImg.load()

		self.alphaMatrix[self.alphaMatrix>self.maxVal] = self.maxVal
		self.alphaMatrix -= self.minVal
		self.alphaMatrix /= float(self.span)
		self.alphaMatrix *= 255
		self.alphaMatrix = np.rint(self.alphaMatrix)

		self.zoomscale = 1
		self.pickedRange = tkinter.StringVar()
		self.pickedRange.set("0")
		self.scaleRange = tkinter.StringVar()
		self.scaleRange.set("%d"%self.maxVal)

		for i in range(self.alphaImg.size[0]):
			for j in range(self.alphaImg.size[1]):
				self.alphaPix[i,j] = (255,255,255,int(self.alphaMatrix[j,i].item()))



		#self.alphaImg.thumbnail((1600,1200))
		#self.masterImg.thumbnail((1600,1200))

		self.photo = ImageTk.PhotoImage(self.masterImg)
		self.mask = ImageTk.PhotoImage(self.alphaImg)
		x = self.masterImg.size[0]//2
		y = self.masterImg.size[1]//2
		self.penRect = self.canvas.create_rectangle(0,0,100,100)
		self.photoCanvasID = self.canvas.create_image(x,y, image=self.photo)
		self.maskCanvasID = self.canvas.create_image(x,y, image=self.mask, state="normal")

		self.mode = "move"
		self.drawVal = (255,0,0,255)
		self.penSize = tkinter.IntVar()
		self.penSize.set(3)
		

		self.cropEvent = None
		self.canvas.bind("<Key>", self.key)
		self.canvas.bind("<Button-1>", self.click)
		self.canvas.bind("<B1-Motion>", self.move)
		self.canvas.bind("<Motion>", self.motion)
		self.canvas.bind("<MouseWheel>",self.zoomer)


		self.container = tkinter.LabelFrame(self.frame)
		self.btnMove = tkinter.Button(self.container, text="Move", command=self.moveClick)
		self.btnDraw = tkinter.Button(self.container, text="Draw", command=self.drawClick)
		self.btnPick = tkinter.Button(self.container, text="Pick", command=self.pickClick)
		self.btnSave = tkinter.Button(self.container, text="Save", command=self.save, state="disabled")
		self.zoomLabel  = tkinter.Label(self.container, width=7, text="Zoom: ")
		self.penLabel  = tkinter.Label(self.container, width=7, text="Pen Size: ")
		self.btnZoomIn = tkinter.Button(self.container, text="+", command=self.zoomIn)
		self.btnZoomOut = tkinter.Button(self.container, text="-", command=self.zoomOut)
		self.btnMaskView = tkinter.Button(self.container, text="Hide Mask", command=self.toggleMask2, width=10)
		self.entDrawRange = tkinter.Entry(self.container, width=10, textvariable=self.pickedRange)
		self.entPenSize = tkinter.Entry(self.container, width=10, textvariable=self.penSize)
		self.entScaleRange = tkinter.Entry(self.container, width=10, textvariable=self.scaleRange)
		self.entScaleRange.bind("<Return>", self.setRange)
		self.cursorRange = tkinter.StringVar()
		self.rangeLabel = tkinter.Label(self.container, width=10, textvariable=self.cursorRange)
		self.activeButton = self.btnMove
		self.container.grid(row=0,column=0)
		self.canvas.grid(row = 1, column = 0)
		self.btnMove.pack(side="left")
		self.btnDraw.pack(side="left")
		self.btnPick.pack(side="left")
		self.btnSave.pack(side="left")
		self.zoomLabel.pack(side="left")
		self.btnZoomIn.pack(side="left")
		self.btnZoomOut.pack(side="left")
		self.btnMaskView.pack(side="left")
		self.drawRangeLabel = tkinter.Label(self.container, width=15, text="Drawing Range:")
		self.drawRangeLabel.pack(side="left")
		self.entDrawRange.pack(side="left")
		self.scaleRangeLabel = tkinter.Label(self.container, width=15, text="Mask Scale Range:")
		self.scaleRangeLabel.pack(side="left")
		self.entScaleRange.pack(side="left")
		self.rangeLabelLabel = tkinter.Label(self.container, width=10, text="Cursor Range:")
		self.penLabel.pack(side="left")
		self.entPenSize.pack(side="left")
		self.rangeLabelLabel.pack(side="left")
		self.rangeLabel.pack(side="right")
		


		self.frame.pack()

		self.canvas.focus_set()
		self.pickClick()

		self.root.mainloop()

	def _deleteWindow(self):
		if self.unsavedChanges:
			if tkMessageBox.askyesno("Save", "Unsaved changes exist.  Would you like to save changes now?"):
				self.save()
		self.root.destroy()

	def setRange(self, evt):
		r = float(self.scaleRange.get())
		self.maxVal = r
		self.scaleRange.set("%d"%r)
		self.minVal = np.min(self.rangeMatrix)
		self.span = self.maxVal-self.minVal

		self.alphaMatrix = np.array(self.rangeMatrix)
		self.alphaMatrix[self.alphaMatrix>self.maxVal] = self.maxVal
		self.alphaMatrix -= self.minVal
		self.alphaMatrix /= float(self.span)
		self.alphaMatrix *= 255
		self.alphaMatrix = np.rint(self.alphaMatrix)




		for i in range(self.alphaImg.size[0]):
			for j in range(self.alphaImg.size[1]):
				self.alphaPix[i,j] = (255,255,255,int(self.alphaMatrix[j,i].item()))
		self.mask = ImageTk.PhotoImage(self.alphaImg)
		self.crop()
		self.canvas.focus_set()


	def changePenSize(self, sizeDelta):
		pSize = self.penSize.get() + sizeDelta
		if pSize < 1: pSize = 1
		self.penSize.set(pSize)

	def key(self, event):
		global mode, penSize, maxVal
		#print "pressed", repr(event.char)
		if event.char == 'p':
			self.pickClick()
		if event.char == "d":
			self.drawClick()
		if event.char == "m":
			self.moveClick()
		if event.char == "b":
			self.changePenSize(1)
		if event.char == "s":
			self.changePenSize(-1)
		if event.char == "v":
			#print "Save Ranges"
			self.save()
		if event.char == 't':
			self.toggleMask()
		


		#print "Mode %s"%self.mode

	def toggleMask2(self):
		#print "Click"
		self.toggleMask()

	def toggleMask(self, event=None, currentState=None):
		if currentState is None:
			currentState = self.canvas.itemcget(self.maskCanvasID, "state")
		if currentState == "hidden":
			self.canvas.itemconfig(self.maskCanvasID, state='normal')
			self.btnMaskView.config(text="Hide Mask")
		elif currentState == "normal":
			self.canvas.itemconfig(self.maskCanvasID, state='hidden')
			self.btnMaskView.config(text="Show Mask")

	def save(self):
		if self.outRangeFile[-4:] == '.npy':
			np.save(self.outRangeFile, self.rangeMatrix)
		elif self.outRangeFile[-4:] == '.bmp':
			maxRange = self.rangeMatrix.max()
			r = np.array(self.rangeMatrix)
			r[r==0] = np.nan
			r *= 254.0/maxRange #scale ranges to 0-254
			r[np.isnan(r)] = 255.0
			red = np.zeros(r.shape)
			rgbArray = np.zeros((r.shape[0],r.shape[1],3), 'uint8')
			rgbArray[..., 0] = red*256 #red
			rgbArray[..., 1] = r #green
			rgbArray[..., 2] = red*256 #blue
			img = Image.fromarray(rgbArray)
			img.save(self.outRangeFile)



		mpimg.imsave(self.outRangeImage, self.rangeMatrix)
		bgImg = Image.new('RGBA', (self.rangeMatrix.shape[1], self.rangeMatrix.shape[0]), "white")
		alpha = self.alphaImg.copy()
		pix = alpha.load()
		for i in range(alpha.size[0]):
			for j in range(alpha.size[1]):
				#if i==1000 and j == 1000:
				#	print "%d to %d"%(pix[i,j][3], 255-pix[i,j][3])
				pix[i,j] = (255,255,255, 255-pix[i,j][3])
		newImg = Image.composite( self.masterImg, bgImg, alpha)
		newImg.save(self.outHazeImage%self.scaleRange.get())
		self.btnSave.config(state='disabled')
		self.unsavedChanges = False



	def moveClick(self):
		self.canvas.focus_set()
		self.activeButton.config(relief="raised")
		self.mode = "move"
		self.activeButton = self.btnMove
		self.activeButton.config(relief="sunken")
		self.canvas.itemconfig(self.penRect, state="hidden")
		self.canvas.config(cursor='sizing')


	def drawClick(self):
		self.activeButton.config(relief="raised")
		self.canvas.focus_set()
		self.mode = "draw"
		self.activeButton = self.btnDraw
		self.activeButton.config(relief="sunken")
		self.canvas.itemconfig(self.penRect, state="normal")
		self.canvas.config(cursor='none')

	def pickClick(self):
		self.canvas.focus_set()
		self.activeButton.config(relief="raised")
		self.mode = "pick"
		self.activeButton = self.btnPick
		self.activeButton.config(relief="sunken")
		self.canvas.itemconfig(self.penRect, state="hidden")
		self.canvas.config(cursor='tcross')

	def canvasToImgCoords(self, x,y):
		ix = self.viewLeft + (x / float(self.masterImg.size[0])) * self.viewWidth
		iy = self.viewTop + (y / float(self.masterImg.size[1])) * self.viewHeight
		return ix,iy

	def imgToCanvasCoords(self, ix,iy):
		x = ((ix - self.viewLeft) / float(self.viewWidth))* float(self.masterImg.size[0]) 
		y = ((iy - self.viewTop) / float(self.viewHeight))* float(self.masterImg.size[1]) 
		return x,y

	def draw(self, event):
		self.btnSave.config(state='normal')
		self.unsavedChanges = True
		self.canvas.focus_set()
		#pix = img.load()
		x, y = self.canvasToImgCoords(event.x, event.y)
		penSize = self.penSize.get()
		x1 = int(x - 0.5*penSize)
		y1 = int(y - 0.5*penSize)
		x2 = int(x + 0.5*penSize)
		y2 = int(y + 0.5*penSize)
		#print "Draw Range %s"%self.pickedRange.get()
		r = float(self.pickedRange.get())
		if r > self.maxVal: r = self.maxVal
		r -= self.minVal
		r /= float(self.span)
		r *= 255
		r = int(r)
		self.drawVal = (255,255,255,r)
		for i in range(x1,x2):
			for j in range(y1,y2):
				self.alphaPix[i, j] = self.drawVal
				self.rangeMatrix[j, i] = float(self.pickedRange.get())
		del self.mask
		if self.cropEvent is None: self.cropEvent = event
		cx1, cy1 = self.imgToCanvasCoords(x1, y1)
		cx2, cy2 = self.imgToCanvasCoords(x2, y2)
		self.canvas.coords(self.penRect, cx1, cy1, cx2, cy2)
		self.crop()

	def motion(self, event):
		x, y = self.canvasToImgCoords(event.x, event.y)
		x = int(x)
		y = int(y)
		if x < self.rangeMatrix.shape[1] and y < self.rangeMatrix.shape[0] and y >=0 and x >=0:
			self.cursorRange.set("%0.2f"%self.rangeMatrix[y,x])
		else:
			self.cursorRange.set("---")
		if self.mode == "draw":
			penSize = self.penSize.get()
			x1 = int(x - 0.5*penSize)
			y1 = int(y - 0.5*penSize)
			x2 = int(x + 0.5*penSize)
			y2 = int(y + 0.5*penSize)
			cx1, cy1 = self.imgToCanvasCoords(x1, y1)
			cx2, cy2 = self.imgToCanvasCoords(x2, y2)
			self.canvas.tag_raise(self.penRect)
			self.canvas.coords(self.penRect, cx1, cy1, cx2, cy2)
			#print "%d %d"%(cx1, cy1)

	def move(self, event):
		#print "Move in mode: %s"%self.mode
		if self.mode == "draw":
			self.draw(event)
		if self.mode == "move":
			#self.canvas.scan_dragto(event.x, event.y, gain=1)
			x = self.viewLeft + (event.x / float(self.masterImg.size[0])) * self.viewWidth
			y = self.viewTop + (event.y / float(self.masterImg.size[1])) * self.viewHeight
			xoff = x - self.moveClickX
			yoff = y - self.moveClickY
			self.cropEvent.x += xoff
			self.cropEvent.y += yoff
			self.moveClickX = x
			self.moveClickY = y
			self.crop()


	def click(self, event):
		#print "Clicked at", event.x, event.y
		if self.mode == "pick":
			x = int(self.viewLeft + (event.x / float(self.masterImg.size[0])) * self.viewWidth)
			y = int(self.viewTop + (event.y / float(self.masterImg.size[1])) * self.viewHeight)
			self.drawVal = self.alphaPix[x,y]
			self.pickedRange.set("%d"%self.rangeMatrix[y,x])
		if self.mode == "move":
			self.moveClickX = self.viewLeft + (event.x / float(self.masterImg.size[0])) * self.viewWidth
			self.moveClickY = self.viewTop + (event.y / float(self.masterImg.size[1])) * self.viewHeight
		if self.mode == "draw":
			self.draw(event)

	def zoomIn(self):
		class obv(object):
			def __init__(self, d):
				self.__dict__ = d

		e = obv({"delta":1, "x":self.masterImg.size[0]/ 2.0, "y":self.masterImg.size[1]/2.0})
		self.zoomer(e)
	def zoomOut(self):
		class obv(object):
			def __init__(self, d):
				self.__dict__ = d

		e = obv({"delta":-1, "x":self.masterImg.size[0]/ 2.0, "y":self.masterImg.size[1]/2.0})
		self.zoomer(e)

	def zoomer(self,event):
		if (event.delta > 0):
			if self.zoomscale != 20: self.zoomscale += 1
		elif (event.delta < 0):
			if self.zoomscale != 1: self.zoomscale -= 1
		#print self.zoomscale
		if self.cropEvent is None: 
			self.cropEvent = event
		self.cropEvent.x = int(self.viewLeft + (event.x / float(self.masterImg.size[0])) * self.viewWidth)
		self.cropEvent.y = int(self.viewTop + (event.y / float(self.masterImg.size[1])) * self.viewHeight) 

		self.crop()

	def crop(self):
		event = self.cropEvent
		if self.photoCanvasID: self.canvas.delete(self.photoCanvasID)
		oldState = "normal"
		if self.maskCanvasID : 
			oldState = self.canvas.itemcget(self.maskCanvasID, "state")
			if oldState == '': oldState = "normal"
			self.canvas.delete(self.maskCanvasID)
		x=y=0
		if event:
			x,y = self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)
		
		size = self.masterImg.size
		xoff = int(size[0] / self.zoomscale)
		yoff = int(size[1] / self.zoomscale)
		self.viewLeft = int(x-xoff)
		self.viewRight = int(x+xoff)
		if self.viewLeft < 0:
			self.viewLeft = 0
			self.viewRight = 2*xoff
		if self.viewRight > self.masterImg.size[0]:
			self.viewRight = self.masterImg.size[0]
			self.viewLeft = self.viewRight - 2*xoff

		self.viewTop = int(y-yoff)
		self.viewBottom = int(y+yoff)
		if self.viewTop<0:
			self.viewTop = 0
			self.viewBottom = 2*yoff
		if self.viewBottom > self.masterImg.size[1]:
			self.viewBottom = self.masterImg.size[1]
			self.viewTop = self.viewBottom - 2*yoff

		self.viewWidth = self.viewRight - self.viewLeft
		self.viewHeight = self.viewBottom - self.viewTop

		#print xoff
		#print yoff
		if self.zoomscale == 1:
			tmp = self.masterImg
			tmpA = self.alphaImg
			self.viewLeft = 0
			self.viewRight = self.masterImg.size[0]
			self.viewTop = 0
			self.viewBottom = self.masterImg.size[1]
			self.viewWidth = self.viewRight - self.viewLeft
			self.viewHeight = self.viewBottom - self.viewTop
		else:
			tmp = self.masterImg.crop((self.viewLeft,self.viewTop,self.viewRight,self.viewBottom))
			tmpA = self.alphaImg.crop((self.viewLeft,self.viewTop,self.viewRight,self.viewBottom))

		self.photo = ImageTk.PhotoImage(tmp.resize(size))
		self.mask = ImageTk.PhotoImage(tmpA.resize(size))
		x = self.masterImg.size[0]/2.0
		y = self.masterImg.size[1]/2.0
		self.photoCanvasID = self.canvas.create_image(x,y,image=self.photo)
		self.maskCanvasID = self.canvas.create_image(x,y,image=self.mask)
		self.canvas.itemconfig(self.maskCanvasID, state=oldState)
		self.canvas.tag_raise(self.penRect)



if __name__ == "__main__":
	r = RangeFixer(imageFile='./GrandCanyon1/GRCA201301160930_reduced.png', rangeFile='./GrandCanyon1/arrays/reduced.npy', outRangeImage='./GrandCanyon1/cleaned.png')
	#r = RangeFixer(imageFile='./GrandCanyon1/GRCA201301160930.jpg', rangeFile='./GrandCanyon1/arrays/range.obsLat_36.06608900.obsLon_-112.11761800.obsElev_2159.000.horizAngle_44.000.vertAngle_20.000.elevAngle_-4.000.heading_303.000.height_1200.width_1600.3_rectified.npy', outRangeImage='./GrandCanyon1/cleaned.png')
	#r = RangeFixer(imageFile='./GrandCanyon1/GRCA201301160930.jpg', rangeFile='./GrandCanyon1/arrays/range.obsLat_36.06608900.obsLon_-112.11761800.obsElev_2159.000.horizAngle_44.000.vertAngle_20.000.elevAngle_-4.000.heading_303.000.height_1200.width_1600.3_rectified_FINAL.npy', outRangeImage='./GrandCanyon1/cleaned.png')